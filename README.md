# gofu

## simple file share service in go

nginx example config:

```
limit_req_zone $binary_remote_addr zone=limits:10m rate=10r/s;

server {
  listen 80;
  listen [::]:80;
  server_name paste.domain.tld;
  return 301 https://$server_name$request_uri;
}

server {
  listen 443 ssl http2;
  listen [::]:443 ssl http2;
  server_name paste.domain.tld;

  client_max_body_size 35M; # set desired max file size
  limit_req zone=limits burst=20 nodelay;
  limit_req_log_level warn;

  ssl_certificate /etc/letsencrypt/live/paste.domain.tld/fullchain.pem;
  ssl_certificate_key /etc/letsencrypt/live/paste.domain.tld/privkey.pem;
  
  location / {
    proxy_pass http://127.0.0.1:9090;
    #proxy_pass  http://unix:/path/to/socket.sock;

    proxy_set_header Host $host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-Proto $scheme;
  }
}
```
