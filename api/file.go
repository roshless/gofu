package api

import (
	"errors"
	"fmt"
	"io"
	"net/http"
	"strconv"
	"sync"
	"time"

	"git.roshless.me/gofu/backend/file"
	"git.roshless.me/gofu/backend/helpers"
	"git.roshless.me/gofu/backend/middleware/jwt"
	"git.roshless.me/gofu/backend/middleware/pagination"
	"git.roshless.me/gofu/backend/model"
	"git.roshless.me/gofu/backend/payloads"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/render"
)

type FileHandler struct {
	model model.FileModel
	j     jwt.JWTObject
}

func (f FileHandler) Routes() chi.Router {
	r := chi.NewRouter()

	r.Group(func(r chi.Router) {
		r.Use(f.j.Verifier())
		r.Use(jwt.AuthenticatorIsLoggedIn)

		r.Post("/", f.create)
		r.With(f.paginate).Get("/", f.listAll)

		r.Route("/{file_name}", func(r chi.Router) {
			r.Delete("/", f.delete)

			r.Route("/content", func(r chi.Router) {
				r.Use(middleware.AllowContentType("multipart/form-data"))

				r.Post("/", f.addContent)
			})
		})

		r.Group(func(r chi.Router) {
			r.Use(jwt.AuthenticatorIsAdmin)

			r.With(f.paginate).Get("/all", f.listEvery)
			r.Get("/fixContentTypes", f.fixContentTypes)
		})
	})

	r.Get("/{file_name}/content", f.getContent)
	r.Get("/{file_name}", f.get)

	return r
}

func (fh FileHandler) listAll(w http.ResponseWriter, r *http.Request) {
	userID := jwt.GetUserID(r.Context())
	pageID := r.Context().Value(pagination.PageIDKey).(int) - 1
	entriesPerPage := r.Context().Value(pagination.PerPageIDKey).(int)

	files, err := fh.model.GetAll(r.Context(), userID, pageID*entriesPerPage, entriesPerPage)
	if err != nil {
		render.Render(w, r, payloads.InternalServerError(err))
		return
	}

	if err := render.RenderList(w, r, payloads.NewFileListResponse(files)); err != nil {
		render.Render(w, r, payloads.InternalServerError(err))
		return
	}
}

func (fh FileHandler) listEvery(w http.ResponseWriter, r *http.Request) {
	pageID := r.Context().Value(pagination.PageIDKey).(int) - 1
	entriesPerPage := r.Context().Value(pagination.PerPageIDKey).(int)

	files, err := fh.model.GetEveryFile(r.Context(), pageID*entriesPerPage, entriesPerPage)

	if err != nil {
		render.Render(w, r, payloads.InternalServerError(err))
		return
	}

	if err := render.RenderList(w, r, payloads.NewFileListResponse(files)); err != nil {
		render.Render(w, r, payloads.InternalServerError(err))
		return
	}
}

func (fh FileHandler) create(w http.ResponseWriter, r *http.Request) {
	f := payloads.FileRequest{}
	if err := render.Bind(r, &f); err != nil {
		render.Render(w, r, &payloads.ErrorResponse{
			Err: err, HTTPStatusCode: http.StatusBadRequest, Error: err.Error()},
		)
		return
	}
	f.SubmitterID = jwt.GetUserID(r.Context())

	var err error

	f.SubmitterIP, err = helpers.GetIPAdress(r)
	if err != nil {
		render.Render(w, r, payloads.InternalServerError(err))
		return
	}

	// TODO should this be set here?
	f.UploadDate = time.Now()

	err = fh.model.Add(r.Context(), f.File)
	if err != nil {
		if errors.Is(err, model.ErrConflict) {
			render.Render(w, r, &payloads.ErrorResponse{
				Err: err, HTTPStatusCode: http.StatusConflict,
				Error: http.StatusText(http.StatusConflict)},
			)
		} else {
			render.Render(w, r, payloads.InternalServerError(err))
		}
		return
	}

	if err = render.Render(w, r, payloads.NewFileResponse(*f.File)); err != nil {
		render.Render(w, r, payloads.InternalServerError(err))
		return
	}
	w.WriteHeader(http.StatusCreated)
}

func (fh FileHandler) addContent(w http.ResponseWriter, r *http.Request) {
	userID := jwt.GetUserID(r.Context())

	fileName := chi.URLParam(r, "file_name")

	f, err := fh.model.GetWithAuth(r.Context(), fileName, strconv.Itoa(userID))
	if err == model.ErrNotFound {
		render.Render(w, r, payloads.NotFound(err))
		return
	} else if err != nil {
		render.Render(w, r, payloads.BadRequest(err))
		return
	}

	if f.IsUploaded {
		err = errors.New("file already uploaded")
		render.Render(w, r, &payloads.ErrorResponse{
			Err: err, HTTPStatusCode: http.StatusBadRequest, Error: err.Error()},
		)
		return
	}

	formFile, formHeader, err := r.FormFile("file")
	if err != nil {
		if formFile == nil {
			render.Render(w, r, &payloads.ErrorResponse{
				Err: err, HTTPStatusCode: http.StatusInternalServerError, Error: "missing *file* form part",
			})
			return
		}
		render.Render(w, r, payloads.InternalServerError(err))
		return
	}

	body, err := io.ReadAll(formFile)
	if err != nil {
		render.Render(w, r, payloads.InternalServerError(err))
		return
	}

	if len(body) == 0 {
		err = errors.New("empty body")
		render.Render(w, r, &payloads.ErrorResponse{
			Err: err, HTTPStatusCode: http.StatusBadRequest, Error: err.Error()},
		)
		return
	}

	if err = file.WriteContent(&f, &body); err != nil {
		render.Render(w, r, payloads.InternalServerError(err))
		return
	}

	contentType := formHeader.Header.Get("Content-Type")
	if contentType == "application/octet-stream" {
		// Attempt to check if this is really just random bytes
		contentType = http.DetectContentType(body)
	}
	err = fh.model.PostUploadFileUpdate(r.Context(), userID, fileName, contentType)
	if err != nil {
		render.Render(w, r, payloads.InternalServerError(err))
		return
	}

	w.WriteHeader(http.StatusOK)
}

func (fh FileHandler) get(w http.ResponseWriter, r *http.Request) {
	f, err := fh.model.Get(r.Context(), chi.URLParam(r, "file_name"))
	if err == model.ErrNotFound {
		render.Render(w, r, payloads.NotFound(err))
		return
	} else if err != nil {
		render.Render(w, r, payloads.BadRequest(err))
		return
	}

	if err = render.Render(w, r, payloads.NewFileResponse(f)); err != nil {
		render.Render(w, r, payloads.InternalServerError(err))
		return
	}
}

func (fh FileHandler) getContent(w http.ResponseWriter, r *http.Request) {
	fileName := chi.URLParam(r, "file_name")

	f, err := fh.model.Get(r.Context(), fileName)
	if err != nil {
		render.Render(w, r, payloads.InternalServerError(err))
		return
	}

	if f.Name == "" {
		render.Render(w, r, payloads.NotFound(errors.New("file not found")))
		return
	} else if !f.IsUploaded {
		render.Render(w, r, payloads.NotFound(errors.New("file not uploaded")))
		return
	}

	contentBytes, err := file.ReadContent(&model.File{Name: fileName})
	if err != nil {
		render.Render(w, r, payloads.InternalServerError(err))
		return
	}

	w.Write(contentBytes)
	w.WriteHeader(http.StatusOK)
}

func (fh FileHandler) delete(w http.ResponseWriter, r *http.Request) {
	userID := jwt.GetUserID(r.Context())
	fileName := chi.URLParam(r, "file_name")

	f, err := fh.model.Get(r.Context(), chi.URLParam(r, "file_name"))
	if err == model.ErrNotFound {
		render.Render(w, r, payloads.NotFound(errors.New("file not found")))
		return
	} else if err != nil {
		render.Render(w, r, payloads.BadRequest(err))
		return
	}

	rowsAffected, err := fh.model.Delete(r.Context(), fileName, strconv.Itoa(userID))
	if err != nil {
		render.Render(w, r, payloads.BadRequest(err))
		return
	}
	if rowsAffected != 1 {
		// Should never happen
		render.Render(w, r, payloads.InternalServerError(err))
		return
	}

	if f.IsUploaded {
		if err = file.DeleteContent(&model.File{Name: fileName}); err != nil {
			render.Render(w, r, payloads.InternalServerError(err))
			return
		}
	}
}

func (fh FileHandler) fixContentTypes(w http.ResponseWriter, r *http.Request) {
	files, err := fh.model.GetEveryFile(r.Context(), 0, 100000)
	if err != nil {
		render.Render(w, r, payloads.InternalServerError(err))
		return
	}

	// TODO is this a good place for something like this?
	// Or should this just trigger the task, and on another endpoint it would end
	// I guess its fine for now
	var (
		wg       sync.WaitGroup
		filesOK  int
		filesBAD int
	)

	for _, f := range files {
		wg.Add(1)
		go func(f model.File) {
			defer wg.Done()

			contentType, err := file.DetectContentType(&f)
			if err != nil {
				filesBAD += 1
				return
			}
			if err = fh.model.UpdateContentType(r.Context(), f.Name, contentType); err != nil {
				filesBAD += 1
				return
			}
			filesOK += 1
		}(f)
	}
	wg.Wait()

	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "OK: %d files, BAD: %d files\n", filesOK, filesBAD)
}

func (fh FileHandler) paginate(next http.Handler) http.Handler {
	return pagination.Paginate(next)
}
