package helpers

import (
	"errors"
	"net"
	"net/http"
	"net/netip"
	"slices"
	"strings"
)

func GetIPAdress(r *http.Request) (netip.Addr, error) {
	for _, h := range []string{"X-Forwarded-For", "X-Real-Ip"} {
		addresses := strings.Split(r.Header.Get(h), ",")
		slices.Reverse(addresses)

		for _, el := range addresses {
			ip := strings.TrimSpace(el)

			realIP := net.ParseIP(ip)
			if !realIP.IsGlobalUnicast() || realIP.IsPrivate() {
				// private address, go to next
				continue
			}
			return netip.ParseAddr(ip)
		}
	}

	return netip.Addr{}, errors.New("ip address in headers not found")
}
