package metrics

import (
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

type MetricsMiddleware struct {
	reqs    *prometheus.CounterVec
	latency *prometheus.HistogramVec
}

func NewMiddleware() func(next http.Handler) http.Handler {
	var m MetricsMiddleware
	m.reqs = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "molo_http_requests_total",
			Help: "How many HTTP requests processed, partitioned by status code, method and HTTP path.",
		},
		[]string{"code", "method", "path"},
	)
	prometheus.MustRegister(m.reqs)

	m.latency = prometheus.NewHistogramVec(prometheus.HistogramOpts{
		Name:    "molo_http_request_duration_seconds",
		Help:    "How long it took to process the request, partitioned by status code, method and HTTP path.",
		Buckets: []float64{300, 1200, 5000},
	},
		[]string{"code", "method", "path"},
	)
	prometheus.MustRegister(m.latency)
	return m.handler
}

func (c MetricsMiddleware) handler(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()
		ww := middleware.NewWrapResponseWriter(w, r.ProtoMajor)
		next.ServeHTTP(ww, r)

		rctx := chi.RouteContext(r.Context())
		if rctx == nil {
			return
		}
		routePattern := strings.Join(rctx.RoutePatterns, "")
		routePattern = strings.ReplaceAll(routePattern, "/*/", "/")

		c.reqs.WithLabelValues(strconv.Itoa(ww.Status()), r.Method, routePattern).Inc()
		c.latency.WithLabelValues(strconv.Itoa(ww.Status()), r.Method, routePattern).Observe(float64(time.Since(start).Nanoseconds()) / 1000000)
	})
}

var (
	UpdatedFeedsCounter = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "molo_updated_feeds_counter",
		Help: "The total number of updated feeds",
	}, []string{"userID", "status"})
)

func Handler() http.Handler {
	return promhttp.Handler()
}

