package controller

import (
	"log/slog"
	"net/http"
	"time"

	"git.roshless.me/gofu/backend/api"
	"git.roshless.me/gofu/backend/config"
	"git.roshless.me/gofu/backend/middleware/jwt"
	"git.roshless.me/gofu/backend/middleware/metrics"
	"git.roshless.me/gofu/backend/model"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/cors"
	"github.com/go-chi/httplog/v2"
)

type Controller struct {
	Router   *chi.Mux
	Config   config.Config
	Models   model.Models
	Handlers api.Handlers
	JWT      jwt.JWTObject
}

func NewController(cfg *config.Config, pool model.PgxIface) *Controller {
	c := &Controller{}
	c.Router = chi.NewRouter()
	c.Config = *cfg
	c.JWT = jwt.NewJWT(cfg.JWTSecretKey)
	c.Models = model.NewModels(pool)
	c.Handlers = api.NewHandlers(cfg, &c.Models, &c.JWT)
	return c
}

func (c *Controller) MountHandlers() {
	logger := httplog.NewLogger("logger", httplog.Options{
		JSON:             true,
		LogLevel:         slog.LevelInfo,
		Concise:          false,
		RequestHeaders:   true,
		MessageFieldName: "message",
		TimeFieldFormat:  time.RFC3339,
	})

	c.Router.Use(middleware.Heartbeat("/up"))

	c.Router.Use(cors.Handler(cors.Options{
		// AllowedOrigins:   []string{"https://foo.com"}, // Use this to allow specific origin hosts
		AllowedOrigins: []string{"https://*", "http://*"},
		AllowOriginFunc:  func(r *http.Request, origin string) bool { return true },
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		ExposedHeaders:   []string{"Link"},
		AllowCredentials: true,
		MaxAge:           300, // Maximum value not ignored by any of major browsers
	}))

	c.Router.Use(middleware.RealIP)
	// RequestLogger automatically makes use of the chi RequestID and Recoverer middleware.
	c.Router.Use(httplog.RequestLogger(logger, []string{
		"/up",
		"/metrics",
	}))
	c.Router.Use(metrics.NewMiddleware())

	c.Router.Mount("/api/v1/user", c.Handlers.Users.Routes())
	c.Router.Mount("/api/v1/file", c.Handlers.Files.Routes())
	c.Router.Mount("/metrics", metrics.Handler())
}
